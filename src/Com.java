import com.google.common.eventbus.Subscribe;

import java.util.ArrayList;

/**
 *  Communicateur : API permettant aux process de communiquer entre eux
 */
public class Com{


    private Lamport clock;
    private EventBusService bus;
    private ArrayList<Object> BaL = new ArrayList<Object>();

    private static int nbProcess = 0;
    private int id = Com.nbProcess++;

    private boolean hasToken;
    private boolean needToken = false;

    private int countSynchro = 0;
    private boolean isWaiting = false;

    /**
     * Constructeur
     *
     * @param clock process vu comme un Lamport
     */
    public Com(Lamport clock) {
        this.clock = clock;

        this.bus = EventBusService.getInstance();
        this.bus.registerSubscriber(this);
    }


    //---------------------------------------------- BROADCAST -----------------------------------------------------------------------------

    /**
     * Crée un broadCastMessage à partir de l'objet et le poste sur le bus à tout ses subscribers
     *
     * @param objet Contenu du message envoyé sur le bus
     */
    void broadcast(Object objet) {
        BroadCastMessages message = new BroadCastMessages(this.id, objet, this.clock.getClock());
        this.bus.postEvent(message);
    }

    /**
     * Méthode @Subscribe de broadcast.
     * Quand elle reçoit un message, elle incrémente l'horloge de Lamport du processus et met le message dans sa boite à lettre
     *
     * @param broadCastMessage Message reçu
     */
    @Subscribe
    void onBroadCastMessages(BroadCastMessages broadCastMessage){
        if (broadCastMessage.getSender() != this.id) {
        try {
            this.clock.setClock(Math.max(broadCastMessage.getHorlogeLamport(), this.clock.getClock()) + 1);
        }catch(Exception e){
            e.printStackTrace();
        }
            this.BaL.add(broadCastMessage.getContent());
        }
    }

    //---------------------------------------------- DEDICATED MESSAGE ---------------------------------------------------------------------

    /**
     * Envoie un message à un destinataire précis
     *
     * @param object Contenu du message
     * @param to id du destinataire
     */
    void sendTo(Object object, int to){
        DedicatedMessages message = new DedicatedMessages(to, object, this.clock.getClock());
        this.bus.postEvent(message);
    }

    /**
     * Méthode @Subscribe du message dédié.
     * Incrémente l'horloge et ajoute le contenu du message à la boite aux lettres si on est le destinataire
     *
     * @param messageDedie Le message reçu
     * @throws InterruptedException levée par le sémaphore associé à la clock
     */
    @Subscribe
    void onDedicatedMessage(DedicatedMessages messageDedie) throws InterruptedException {
        if (messageDedie.getReceiver() == this.id) {
            this.clock.setClock( Math.max(messageDedie.getHorlogeLamport(), this.clock.getClock()) + 1);
            this.BaL.add(messageDedie.getContent());
        }
    }

    //---------------------------------------------- GESTION CRITICITÉ ---------------------------------------------------------------------

    // Declaration de la methode de callback invoquee lorsqu'un Token transite sur le bus

    /**
     * Méthode @Subscribe du token gerant la section critique
     * Garde le token tant que le processus en a besoin, avant de le renvoyer au Com suivant
     * Il s'agit de messages systèmes n'influant pas sur l'horloge de Lamport
     * Il n'y a pas de méthode d'envoit de token propre au Com car il est envoyé sur le bus à l'initialisation du système
     *
     * @param token
     */
    @Subscribe
    void onToken(Token token){
        if(token.getPosition() == this.id){
            this.hasToken = true;
            do {
                //Attend de recevoir l'indication du process qu'il n'a plus besoin du token
                try{
                    Thread.sleep(200);
                }catch(Exception e){
                    e.printStackTrace();
                }
            } while(this.needToken);
            this.hasToken = false;
            this.bus.postEvent(new Token(next()));
        }
    }

    /**
     * Fourni au process le moyen d'indiquer qu'il veut rentrer en section critique
     */
    void requestToken(){
        this.needToken = true;
    }

    /**
     * Fourni au process le moyen d'indiquer qu'il n'a plus besoin d'être en section critique
     */
    void releaseToken(){
        this.needToken = false;
    }

    /**
     * Fourni au process le moyen de vérifier s'il a effectivement obtenu la section critique
     *
     * @return le booléen indiquant si on a le token ou non
     */
    boolean hasToken() {
        return this.hasToken;
    }

    //---------------------------------------------- SYNCHRONISATION ------------------------------------------------------------------------

    /**
     * Fourni au process le moyen d'indiquer aux autres qu'il est nécessaire de s'attendre
     */
    void synchronize() {
        Synchro synchro = new Synchro();
        this.bus.postEvent(synchro);
        this.isWaiting = true;

        do {
            try{
                Thread.sleep(100);
            }catch(Exception e){
                e.printStackTrace();
            }
        } while(this.countSynchro != nbProcess);
        this.isWaiting = false;

        this.countSynchro = 0;
    }

    /**
     * Méthode @Subscribe du mécanisme de synchronisation
     * Il s'agit de messages systèmes n'influant pas sur l'horloge de Lamport
     * Indique qu'un process s'est arrêté pour se synchroniser
     *
     * @param synchro
     */
    @Subscribe
    void onSynchro(Synchro synchro){
        this.countSynchro++;
        //System.out.println("Com number " + this.id + " increased its countSynchro to : " + this.countSynchro);
    }

    /**
     * Fourni au process le moyen de vérifier s'il doit attendre avant de continuer son exécution
     *
     * @return le booléen indiquant si l'on est en train d'attendre ou non
     */
    boolean isWaiting(){
        return isWaiting;
    }

    /**
     * Fourni au process le moyen de récupérer les messages qui lui ont été adressés
     *
     * @return Un objet stocké dans la boîte aux lettres
     */
    Object fetchMessage() {
        Object message = null;
        if (!this.BaL.isEmpty()){
            message = this.BaL.get(0);
            this.BaL.remove(0);
        }
        return message;
    }

    /**
     *
     * @return l'id du process suivant
     */
    private int next(){
        return (this.id + 1)%Com.nbProcess;
    }

    /**
     * Fourni au process un moyen de détruire son communicateur
     */
    void killCom(){
        this.needToken = false;

        try{
            Thread.sleep(500);
        }catch(Exception e){
            e.printStackTrace();
        }

        this.bus.unRegisterSubscriber(this);
    }
}
