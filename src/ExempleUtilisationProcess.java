/**
 * Main lançant le système : On créer 4 processus qu'on laisse s'exécuter pendant 30 secondes avant de les arrêter
 */
public class ExempleUtilisationProcess {

    public static int nbProcess = 4;

    public static void main(String[] args) throws InterruptedException {

        EventBusService bus = EventBusService.getInstance();

        Process[] px = new Process[nbProcess];
        for (int i = 0; i < px.length; i++) {
            /*
            if (i == idPosseseurToken){
                px[i] = new Process("P" + (i + 1), token);
            }
            else{
                px[i] = new Process("P" + (i + 1), null);
            }
            */
            px[i] = new Process("P" + (i + 1));
        }
        bus.postEvent(new Token(0));

        try{
            Thread.sleep(30000);
        }catch(Exception e){
            e.printStackTrace();
        }

        for (Process aPx : px) {
            aPx.stop();
        }
    }
}