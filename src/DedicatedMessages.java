/**
 * Type de message associé aux messages dédiés
 */
public class DedicatedMessages extends Messages{

    private int receiver;

    /**
     * Constructeur d'un message de type dédié
     *
     * @param receiver L'id du Com auquel on envoit le message
     * @param objet Le contenu du message
     * @param horloge La valeur de l'horloge au moment de l'envoie
     */
    DedicatedMessages(int receiver, Object objet, int horloge){
        this.receiver = receiver;
        this.objet = objet;
        this.horlogeLamport = horloge;
    }

    /**
     * @return Le temps actuel de l'horloge
     */
    int getReceiver() {
        return receiver;
    }
}