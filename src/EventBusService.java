import com.google.common.eventbus.AsyncEventBus;
import com.google.common.eventbus.EventBus;

import java.util.concurrent.Executors;

/**
 * Implémentation du mécanisme de bus permettant aux communicateurs de s'échanger des messages
 */
public class EventBusService {

    private static EventBusService instance = null;

    private EventBus eventBus = null;

    private EventBusService() {
        eventBus = new AsyncEventBus(Executors.newCachedThreadPool());
    }

    public static EventBusService getInstance() {
        if (instance==null){
            instance = new EventBusService();
        }
        return instance;
    }

    public void registerSubscriber(Object subscriber) {
        eventBus.register(subscriber);
    }

    public void unRegisterSubscriber(Object subscriber) {
        eventBus.unregister(subscriber);
    }

    public void postEvent(Object e) {
        try{
            eventBus.post(e);
        }catch(Exception ex){
            ex.printStackTrace();
        }
    }
}